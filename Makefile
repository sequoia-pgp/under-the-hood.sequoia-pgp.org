# Makefile for under-the-hood.sequoia-pgp.org

# Tools.
HUGO		?= hugo
HUGO_FLAGS	?=
RSYNC		?= rsync
RSYNC_FLAGS	?=

# Configuration.
TARGET		?= sequoia-pgp.org:under-the-hood.sequoia-pgp.org

.PHONY: all
all: build

.PHONY: build
build:
	$(HUGO) $(HUGO_FLAGS)

.PHONY: server
server:
	$(HUGO) server

.PHONY: deploy
deploy: build
	$(RSYNC) $(RSYNC_FLAGS) -r public/* $(TARGET)
