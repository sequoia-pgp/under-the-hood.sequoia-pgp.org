+++
title = "Literal Data"
keywords = ["packet", "literal", "payload"]
weight = 90
+++

If you want to encrypt or sign some data using OpenPGP, this data
needs to be enclosed by a literal data packet.  The literal data
packet also contains some metadata, like the format, filename, and a
timestamp.

<figure>
  <div class="packet">
    <div>Frame</div>
    <div>Format</div>
    <div>Filename</div>
    <div>Date</div>
    <div class="expand">Body</div>
  </div>
  <figcaption>A <i>Literal Data Packet</i></figcaption>
</figure>

Note: This metadata has to be taken with a grain of salt, because it
is not protected by *Version 4 Signatures*.  By not including the
metadata (and the framing) in the signature, a signature over some
data enclosed by a literal data packet is the same as a detached
signature over the same data.  The proposed *Version 5 Signatures* do
include the metadata.

{{<openpgp caption="Example <i>Literal Data Packet</i>" hex="on">}}-----BEGIN PGP MESSAGE-----

yx1iCWhlbGxvLnR4dFx1TW9IZWxsbywgd29ybGQhCg==
=3swl
-----END PGP MESSAGE-----
{{</openpgp>}}

The first byte denotes the kind of data.  `0x62`, or `b`, indicates
binary data, `t` text, `u` text encoded using UTF-8, and `m` an
enclosed MIME message body part.  Most of the time, the format will
simply be set to `b`.

The next byte indicates the length of the file name, followed by the
actual file name.  This encoding limits the length of the filename to
256 characters.  The zero-length filename is valid.

Next, a unsigned four byte value denotes a time associated with the
data.  The standard suggests to use the creation or modification time.
The special value `0` means that no specific time should be associated
with the data.

Finally, the remainder of the packet is simply the enclosed data
itself.

<hr />

If you want to learn more about OpenPGP's *Literal Data Packet*, see
[section 5.9 of RFC4880](https://tools.ietf.org/html/rfc4880#section-5.9).
