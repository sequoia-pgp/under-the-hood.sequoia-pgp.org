+++
title = "Packet Structure"
keywords = ["packet", "packets"]
weight = 100
+++

OpenPGP data is organized in packets.  Every packet starts with a
header containing a packet tag, and the length of the packet.  In
these documents, we will call the packet structure, and the packet
headers the *frame*, or *framing*.

<figure>
  <div class="packet">
    <div>CTB</div>
    <div>Length</div>
	<div class="expand">Body</div>
  </div>
  <figcaption>An OpenPGP <i>Packet</i></figcaption>
</figure>

For historic reasons, *CTB* stands for Cipher Type Byte.  Nowadays, it
simply contains the packet's *Tag*, and the *Tag* specifies what kind
of packet we are dealing with.  If an old-style *CTB* is used, then it
also indicates the kind of length that immediately follows the *CTB*.

The packet length specifies the extend of the packet, excluding the
*CTB* and the length information.  For example, consider the following
*User ID Packet*:

{{<openpgp caption="Example <i>User ID Packet</i>" hex="on">}}-----BEGIN PGP ARMORED FILE-----

zQ9mb29AZXhhbXBsZS5vcmc=
=4adU
-----END PGP ARMORED FILE-----
{{</openpgp>}}

If you click on the button in the top-right corner of the above
figure, or use `sq dump` to inspect the above OpenPGP data, you will
see:

```text
New CTB, 15 bytes: User ID Packet
    Value: foo@example.org

00000000  cd 0f                                              frame
00000002        66 6f 6f 40 65 78  61 6d 70 6c 65 2e 6f 72   value
00000010  67
```

The first byte with the value `0xcd`, or `0b1100_1101`.  The most
significant bit tells us that it is a *CTB*, the next bit tells us
that it is a new-style *CTB*, and the remaining bits tell us the
packet's *Tag*, in this case 13, denoting a *User ID Packet*.

The next byte, `0x0f` specifies the packet's length, encoded as a
*One-Octet New Format Packet Lengths*.  In this case, the length of
the packet is 15, excluding the *CTB* and the encoded length itself.

*User ID Packets* have a very simple structure, the body of the packet
is simply the user id encoded using UTF-8.

<hr />

If you want to learn more about OpenPGP's packet structure, see
[section 4 of RFC4880](https://tools.ietf.org/html/rfc4880#section-4).
